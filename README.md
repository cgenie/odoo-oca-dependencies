# Fetch `oca-dependencies`

This program downloads a module's `oca_dependencies.txt` file recursively and
prints out the SaltStack config to add to pillar.

# Compiling

Assuming you have Ocam's [Opam](https://opam.ocaml.org/):

```
opam switch oca-dependencies --alias-of 4.07.0
opam install dune utop ssl tls lwt lwt_ppx lwt_ssl
opam install cohttp cohttp-lwt-unix
dune build oca_dependencies.exe
./_build/default/oca_dependencies.exe <oca-project-name> <odoo-version>
```
