open Cohttp
open Cohttp_lwt_unix

let module_name = ref ""
let version = ref 0

let usage = Printf.sprintf "Usage: %s [-m string] [-v string]" Sys.argv.(0)

let speclist = [
  ("-m", Arg.Set_string module_name, ": name of the OCA module (e.g. server-tools)");
  ("-v", Arg.Set_int version, ": Odoo version (7, 8, 9, 10, 11, 12)");
]

let github_oca_dependencies_url m v = Printf.sprintf "https://raw.githubusercontent.com/OCA/%s/%d.0/oca_dependencies.txt" m v

(* Cache for fetched modules.
   NOTE Cyclic dependencies are allowed. This is because these are projects which can have interleaving dependencies.
        That's why it's a first-level cache only. To get main project's dependencies, you need to traverse this tree. *)
type fetched_module = string * (string list)
let fetched_modules = ref ([] : fetched_module list)

let rec assoc_find k (l : fetched_module list) =
  match l with
    | [] -> None
    | (xk, xks) :: _ when xk = k -> Some xks
    | _ :: xs -> assoc_find k xs

let rec fetch_oca_dependencies m v =
  let%lwt () = Lwt_io.printf "Getting dependencies for %s (version %d)\n" m v in

  let parse_body b =
    let fltr l = let lt = String.trim l in
      ((String.length lt > 0) && (lt.[0] != '#')) in
    let lines = List.filter fltr (String.split_on_char '\n' b) in
    (* List.iter (fun line -> Printf.printf "Line: %s\n" line) lines; *)
    lines
  in

  match (assoc_find m !fetched_modules) with
    | Some deps -> (
      let%lwt () = Lwt_io.printf "Skipping %s\n" m in
      Lwt.return deps)
    | None -> (
      let url = github_oca_dependencies_url m v in
      (* let%lwt () = Lwt_io.printf "Url: %s\n" url in *)
      let%lwt resp, b = Client.get (Uri.of_string url) in
      let code = resp |> Response.status |> Code.code_of_status in
      let%lwt body = b |> Cohttp_lwt.Body.to_string in
        (* let%lwt () = Lwt_io.printf "Code: %d, Body: %s\n" code body in *)
        if code = 200 then
          let lines = parse_body body in
          fetched_modules := (m, lines) :: !fetched_modules;
          let%lwt deps = Lwt_list.map_s (fun lm -> fetch_oca_dependencies lm v) lines in
          let deps_c = List.concat deps in
          Lwt.return deps_c
        else (
          fetched_modules := (m, []) :: !fetched_modules;
          Lwt.return [])
  )

let salt_stack_dep dep v =
  if String.contains dep ' ' then (
    (* Format is: dep-name, git-repo, branch *)
    let dep_git_branch = String.split_on_char ' ' dep in
    Printf.printf "- name: %s\n  git: %s\n  branch: %s\n  parent-dir: .\n" (List.nth dep_git_branch 0) (List.nth dep_git_branch 1) (List.nth dep_git_branch 2)
    )
  else
    Printf.printf "- name: %s\n  git: https://github.com/OCA/%s.git\n  branch: %d.0\n  parent-dir: .\n" dep dep v

let () =
  Arg.parse
    speclist
    (fun x -> raise (Arg.Bad ("Bad argument: " ^ x)))
    usage;

  if (String.length !module_name = 0) then (
    Printf.printf "%s\n" usage;
    exit 1);
  if (!version = 0) then (
    Printf.printf "%s\n" usage;
    exit 1);

  let _ = Lwt_main.run (fetch_oca_dependencies !module_name !version) in
  let deps = List.map (fun (_, ds) -> ds) !fetched_modules |> List.concat |> List.sort_uniq String.compare in
  List.iter (fun dep -> salt_stack_dep dep !version) deps
